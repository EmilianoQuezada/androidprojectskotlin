package com.example.androidprojectsc2

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class operacionesActivity : AppCompatActivity() {
    private lateinit var lblUsuario : TextView
    private lateinit var txtNum1 : EditText
    private lateinit var txtNum2 : EditText
    private lateinit var lblResultado : TextView

    private lateinit var btnSuma : Button
    private lateinit var btnResta : Button
    private lateinit var btnMulti : Button
    private lateinit var btnDiv : Button

    private lateinit var btnLimpiar : Button
    private lateinit var btnRegresar : Button

    private lateinit var operaciones : Operaciones
    var opcion : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventosClick()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes(){
        lblUsuario = findViewById(R.id.lblUsuario)
        lblResultado = findViewById(R.id.lblResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)
        btnMulti = findViewById(R.id.btnMulti)
        btnDiv = findViewById(R.id.btnDiv)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            lblUsuario.text = bundle.getString("nombre", "Desconocido")
        } else {
            lblUsuario.text = "Usuario: Desconocido"
        }

        lblUsuario.text = bundle?.getString("nombre")
    }
    var num1 = 0.0f
    var num2 = 0.0f


    public fun validar(): Boolean{
        if (txtNum1.text.toString().contentEquals("") || txtNum1.text.toString().contentEquals("")){
            Toast.makeText(this,"Por favor ingresa los 2 numeros", Toast.LENGTH_SHORT).show()
            return false
        }else{
            num1 = convertirFloat(txtNum1.text.toString())
            num2 = convertirFloat(txtNum2.text.toString())
            return true
        }
    }

    fun convertirFloat(num: String) : Float{
        try {
            val numero = num.toFloat()
            return numero
        } catch (e: NumberFormatException) {
            Toast.makeText(this, "Solo admite numero", Toast.LENGTH_SHORT).show()
            return 0.0f
        }
    }


    public fun operacion(){
        var num1 : Float = 0.0f
        var num2 : Float = 0.0f
        var res : Float = 0.0f
        if(validar()){
            num1 = txtNum1.text.toString().toFloat();
            num2 = txtNum1.text.toString().toFloat();
            operaciones = Operaciones(num1,num2)
            when(opcion){
                1-> {operaciones.suma()}
                2-> {operaciones.resta()}
                3-> {operaciones.multiplicacion()}
                4-> {operaciones.division()}
            }
        }
        else{
            Toast.makeText(this,"Falto capturar informacion", Toast.LENGTH_SHORT).show()
        }

    }

    fun eventosClick(){

        btnSuma.setOnClickListener(View.OnClickListener {
            if(validar()){
                operaciones = Operaciones(num1, num2)
                lblResultado.text=operaciones.suma().toString()
            }
        })
        btnResta.setOnClickListener(View.OnClickListener {

            if(validar()){
                operaciones = Operaciones(num1, num2)
                lblResultado.text=operaciones.resta().toString()
            }
        })
        btnMulti.setOnClickListener(View.OnClickListener {
            if(validar()){
                operaciones = Operaciones(num1, num2)
                lblResultado.text=operaciones.multiplicacion().toString()
            }
        })
        btnDiv.setOnClickListener(View.OnClickListener {
            if(validar()){
                if(num1.equals(0.0f) || num2.equals(0.0f))
                    Toast.makeText(this, "No se puede dividir un 0", Toast.LENGTH_SHORT).show()
                else{
                    operaciones = Operaciones(num1, num2)
                    lblResultado.text=operaciones.division().toString()
                }
            }
        })
        btnRegresar.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Salir")
                .setMessage("¿Deseas salir al Menu?")

            builder.setPositiveButton("Aceptar") { dialog, which ->
                Toast.makeText(this, "Has salido al menu", Toast.LENGTH_SHORT).show()
                finish()
            }
            builder.setNegativeButton("Cancelar") { dialog, which ->
                Toast.makeText(this, "Has presionado Cancelar.", Toast.LENGTH_SHORT).show()
            }
            val dialog = builder.create()
            dialog.show()
        })
        btnLimpiar.setOnClickListener(View.OnClickListener {
            lblResultado.text=""
            txtNum1.text= Editable.Factory.getInstance().newEditable("")
            txtNum2.text= Editable.Factory.getInstance().newEditable("")
        })
    }
}


