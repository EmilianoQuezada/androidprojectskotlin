package com.example.androidprojectsc2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import android.widget.Button
    class MainActivity : AppCompatActivity() {
        private lateinit var txtUsuario: EditText
        private lateinit var txtPass: EditText
        private lateinit var btnIngresar: Button
        private lateinit var btnSalir: Button

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            enableEdgeToEdge()
            setContentView(R.layout.activity_main)
            //Iniciar Componentes
            iniciarComponentes()
            eventosClick()
            ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
                val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
                insets
            }
        }

        public fun iniciarComponentes() {
            txtUsuario = findViewById(R.id.txtUsuario)
            txtPass = findViewById(R.id.txtPass)
            btnIngresar = findViewById(R.id.btnIngresar) as Button // Castear a Button
            btnSalir = findViewById(R.id.btnSalir) as Button // Castear a Button
        }

        public fun eventosClick() {
            btnIngresar.setOnClickListener(View.OnClickListener {
                val usuario = getString(R.string.usuario)
                val pass = getString(R.string.pass)
                val nombre = getString(R.string.nombre)
                if (txtUsuario.text.toString() == usuario && txtPass.text.toString() == pass) {
                    val intent = Intent(this, operacionesActivity::class.java)
                    intent.putExtra("nombre", nombre)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "El usuario o la contraseña no coinciden", Toast.LENGTH_SHORT).show()
                }
            })

            btnSalir.setOnClickListener(View.OnClickListener {
                finish()
            })
        }
    }
